class Main {
 public static void main(String[] args) {
 int age = 21;
 if(age > 21) {
 System.out.println("You're over 21 years old."); 
}else if(age == 21) { 
System.out.println("You're 21 years old."); 
}else { 
System.out.println("You're below 21 years old."); 
}
           System.out.println(); 
System.out.println("While Loop"); 
int i = 0;
 while(i < 5) {
 System.out.println("berna");
 i++;
 }
 System.out.println("Do-While Loop"); 
i = 0;
 do { 
System.out.println("berna");
 i++;
 }
while(i < 5); 
System.out.println("For Loop");
 for(i = 0; i < 5; i++) { 
System.out.println("berna");
 }
 System.out.println("For-Each Loop");
 int num[] = {10, 15, 18, 36, 16};
 for(int n : num) { 
System.out.println(n); 
} 
     }
 }
